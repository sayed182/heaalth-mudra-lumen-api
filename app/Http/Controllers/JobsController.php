<?php

namespace App\Http\Controllers;

use App\Jobs\Job;
use App\Models\Experience;
use App\Models\Jobs;
use App\Models\PostingAs;
use App\Models\Vacancy;
use Illuminate\Http\Request;

class JobsController extends Controller
{
    //
    public function index(Request $request){
        $jobs = Job::all();
        $data = [];
        foreach ($jobs as $job){
            $vacancy = Vacancy::query()->findOrFail($job->vacancy_id)->first;
            $experience = Experience::query()->findOrFail($job->experience_id)->first;
            $postedBy = PostingAs::query()->findOrFail($job->posting_as_id)->first;
        }

        return \response()->json(['status'=> 201, 'message'=>'success', 'data' => Jobs::all()], '200');
    }
    public function getById($id, Request $request){
        try {
            $post = Jobs::query()->findOrFail($id);
            return \response()->json(['status'=> 201, 'message'=>'success', 'data' => $post], '200');
        }catch (\Exception $e) {
            return \response()->json(['status' => 401, 'message' => 'No posts with the matching ID']);
        }
    }

    public function update($id, Request $request){
        $data = $request->input()['data'];
        print_r($data);
        try{
            $post = Jobs::query()->where('id',$id)->update($data);
            print_r($post);
            return \response(['status' => 201, 'message' => 'Successfully Updated '.$id.' with '.json_encode($data)]);
        }catch (\Exception $e){
            return \response(['status' => 401, 'message' => `Error Updating. $e `]);
        }
    }

    public function create(Request $request){
        $inputs = $request->input()['data'];
        try{
            $post = Jobs::query()->create($inputs);
            $post->saveOrFail();
            return \response(['status' => 201, 'message' => 'Successfully Created '.json_encode($inputs)]);
        }catch(\Exception $e){
            return \response(['status' => 401, 'message' => `Error Updating. $e`]);
        }
    }

    public function delete($id,Request $request){
        try{
            Posts::query()->where('id', $id)->delete();
            return \response(['status' => 201, 'message' => 'Successfully Deleted '.$id]);
        }catch (\Exception $e){
            return \response(['status' => 401, 'message' => `Error Updating. $e`]);
        }
    }
}
