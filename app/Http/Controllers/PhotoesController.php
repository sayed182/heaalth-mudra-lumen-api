<?php

namespace App\Http\Controllers;

use App\Models\Photoes;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class PhotoesController extends Controller
{

    public function index(Request $request){
        return \response()->json(['status'=> 201, 'message'=>'success', 'data' => Photoes::all()], '200');
    }
    public function getById($id, Request $request){
        try {
            $post = Photoes::query()->findOrFail($id);
            return \response()->json(['status'=> 201, 'message'=>'success', 'data' => $post], '200');
        }catch (\Exception $e) {
            return \response()->json(['status' => 401, 'message' => 'No posts with the matching ID']);
        }
    }

    public function update($id, Request $request){
        $data = $request->input()['data'];
        print_r($data);
        try{
            $post = Photoes::query()->where('id',$id)->update($data);
            print_r($post);
            return \response(['status' => 201, 'message' => 'Successfully Updated '.$id.' with '.json_encode($data)]);
        }catch (\Exception $e){
            return \response(['status' => 401, 'message' => `Error Updating. $e `]);
        }
    }

    public function create(Request $request){
        $inputs = $request->input()['data'];
        try{
            $post = Photoes::query()->create($inputs);
            $post->saveOrFail();
            return \response(['status' => 201, 'message' => 'Successfully Created '.json_encode($inputs)]);
        }catch(\Exception $e){
            return \response(['status' => 401, 'message' => `Error Updating. $e`]);
        }
    }

    public function delete($id,Request $request){
        try{
            Photoes::query()->where('id', $id)->delete();
            return \response(['status' => 201, 'message' => 'Successfully Deleted '.$id]);
        }catch (\Exception $e){
            return \response(['status' => 401, 'message' => `Error Updating. $e`]);
        }
    }

    public function upload(Request $request){
        if($request->hasFile('image')){
            $images = $request->file('image');
            $ids=[];
            foreach ($images as $image){
                try{
                     //getExtension() Dosent work.
                    $name = $image->getBasename().'.'.$image->extension();
                    $path = '';
                    try{
                        $path = $image->storeAs('public', $name);
                    }catch (\Exception $exception){
                        return \response(['status' => 401, 'message' => 'File Not Uploaded']);
                    }
                    $data = Photoes::query()->create(['url'=>$path]);
                    $data->save();
                    array_push($ids, $data->id);
                }catch(\Exception $e){
                    return \response(['status' => 401, 'message' => `Error Updating. $e`]);
                }
            }
            return $ids;
        }
        return null;
    }
}
