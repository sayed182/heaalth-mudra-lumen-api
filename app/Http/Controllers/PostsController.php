<?php

namespace App\Http\Controllers;

use App\Models\Photoes;
use App\Models\Posts;
use App\Models\User;
use http\Env\Response;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    //
    public function index(Request $request){
        $posts = Posts::all();
        $data = [];
        foreach ($posts as $post){
            $user = User::query()->findOrFail($post->user_id);
            $post['user']=$user;
            $post['thumb_url'] = Photoes::query()->find($post->photo_id['id']);
            array_push($data, $post);
        }

        return \response()->json(['status'=> 201, 'message'=>'success', 'data' => $data], '200');
    }
    public function getById($id, Request $request){
        try {
            $post = Posts::query()->findOrFail($id);
            return \response()->json(['status'=> 201, 'message'=>'success', 'data' => $post], '200');
        }catch (\Exception $e) {
            return \response()->json(['status' => 401, 'message' => 'No posts with the matching ID']);
        }
    }

    public function update($id, Request $request){
        $data = $request->input()['data'];
        print_r($data);
        try{
            $post = Posts::query()->where('id',$id)->update($data);
            print_r($post);
            return \response(['status' => 201, 'message' => 'Successfully Updated '.$id.' with '.json_encode($data)]);
        }catch (\Exception $e){
            return \response(['status' => 401, 'message' => `Error Updating. $e `]);
        }
    }

    public function create(Request $request){
        $inputs = $request->input()['data'];
        try{
            $post = Posts::query()->create($inputs);
            $post->saveOrFail();
            return \response(['status' => 201, 'message' => 'Successfully Created '.json_encode($inputs)]);
        }catch(\Exception $e){
            return \response(['status' => 401, 'message' => `Error Updating. $e`]);
        }
    }

    public function delete($id,Request $request){
        try{
            Posts::query()->where('id', $id)->delete();
            return \response(['status' => 201, 'message' => 'Successfully Deleted '.$id]);
        }catch (\Exception $e){
            return \response(['status' => 401, 'message' => `Error Updating. $e`]);
        }
    }
}
