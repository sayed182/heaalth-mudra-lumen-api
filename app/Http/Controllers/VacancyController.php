<?php

namespace App\Http\Controllers;

use App\Models\Vacancy;
use Illuminate\Http\Request;

class VacancyController extends Controller
{
    public function index(Request $request){
        return \response()->json(['status'=> 201, 'message'=>'success', 'data' => Vacancy::all()], '200');
    }
    public function getById($id, Request $request){
        try {
            $post = Vacancy::query()->findOrFail($id);
            return \response()->json(['status'=> 201, 'message'=>'success', 'data' => $post], '200');
        }catch (\Exception $e) {
            return \response()->json(['status' => 401, 'message' => 'No posts with the matching ID']);
        }
    }

    public function update($id, Request $request){
        $data = $request->input()['data'];
        print_r($data);
        try{
            $post = Vacancy::query()->where('id',$id)->update($data);
            print_r($post);
            return \response(['status' => 201, 'message' => 'Successfully Updated '.$id.' with '.json_encode($data)]);
        }catch (\Exception $e){
            return \response(['status' => 401, 'message' => `Error Updating. $e `]);
        }
    }

    public function create(Request $request){
        $inputs = $request->input()['data'];
        try{
            $post = Vacancy::query()->create($inputs);
            $post->saveOrFail();
            return \response(['status' => 201, 'message' => 'Successfully Created '.json_encode($inputs)]);
        }catch(\Exception $e){
            return \response(['status' => 401, 'message' => `Error Updating. $e`]);
        }
    }

    public function delete($id,Request $request){
        try{
            Vacancy::query()->where('id', $id)->delete();
            return \response(['status' => 201, 'message' => 'Successfully Deleted '.$id]);
        }catch (\Exception $e){
            return \response(['status' => 401, 'message' => `Error Updating. $e`]);
        }
    }
}
