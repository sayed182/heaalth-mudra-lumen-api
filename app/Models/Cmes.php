<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cmes extends Model
{
    use HasFactory;
    //
    protected $fillable=[
        'posting_as_id', 'organising', 'topic', 'person_name', 'description', 'inclusion', 'offline_online', 'number_of_days',
        'place', 'venue', 'fees', 'registration_authority', 'contact_number', 'email', 'photo_id', 'service_charges_amount', 'service_charges_days'
    ];
    protected $casts =[
        'photo_id'=>'array'
    ];

    public function photoes(){
        return $this->hasMany(Photoes::class);
    }
}
