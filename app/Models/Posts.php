<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    use HasFactory;
    //
    protected $fillable =[
      'type', 'post', 'photo_id', 'youtube_link'
    ];
    protected $casts=[
        'photo_id'=>'array'
    ];

    public function photo(){
        return $this->hasMany(Photoes::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
}
