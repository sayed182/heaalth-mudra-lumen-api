<?php

namespace Database\Factories;

use App\Models\Cmes;
use Illuminate\Database\Eloquent\Factories\Factory;

class CmesFactory extends Factory
{
    protected $model = Cmes::class;

    public function definition(): array
    {
    	return [
            'posting_as_id' => $this->faker->numberBetween(0,3),
            'organising' => $this->faker->randomElement(['academy', 'college', 'agency']),
            'topic' => $this->faker->randomAscii,
            'person_name' => $this->faker->name,
            'description' => $this->faker->text(),
            'inclusion' => $this->faker->word,
            'offline_online' => $this->faker->randomElement([true, false]),
            'number_of_days' => $this->faker->randomDigit,
            'place' => $this->faker->city,
            'venue' => $this->faker->address,
            'fees' => $this->faker->randomElement([true, false]),
            'registration_authority' => $this->faker->company,
            'contact_number' => $this->faker->phoneNumber,
            'email' => $this->faker->email,
            'photo_id'=>[
                'id'=>$this->faker->numberBetween(1,20)
            ],
    	];
    }
}
