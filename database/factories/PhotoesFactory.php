<?php

namespace Database\Factories;

use App\Model;
use App\Models\Photoes;
use Illuminate\Database\Eloquent\Factories\Factory;

class PhotoesFactory extends Factory
{
    protected $model = Photoes::class;

    public function definition(): array
    {
    	return [
            'url'=>$this->faker->imageUrl(),
    	];
    }
}
