<?php

namespace Database\Factories;

use App\Models\Posts;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostsFactory extends Factory
{
    protected $model = Posts::class;

    public function definition(): array
    {
    	return [
    	    'type' => $this->faker->randomElement(['clinical', 'theory']),
            'post' => $this->faker->paragraph(2),
            'photo_id'=>[
                'id'=>$this->faker->numberBetween(1,20)
            ],
            'user_id'=>User::all()->random()->pluck('id'),
            'youtube_link' => 'https://youtu.be/bmVKaAV_7-A',
    	];
    }
}
