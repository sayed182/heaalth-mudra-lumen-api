<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'username' => $this->faker->unique()->name,
            'mobile' => $this->faker->phoneNumber,
            'profile_picture' => $this->faker->imageUrl(),
            'followers' => $this->faker->randomNumber(),
            'carrier_details' => $this->faker->domainName,
            'course_specialization' => $this->faker->randomElement(['medical','dental', 'phisio']),
            'degree' => $this->faker->randomElement(['MBBS', 'BDS']),
            'city' => $this->faker->city,
            'state' =>$this->faker->state,
            'country' => $this->faker->country,
        ];
    }
}
