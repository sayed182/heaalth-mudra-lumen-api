<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username')->unique();
            $table->string('mobile')->unique();
            $table->string('profile_picture');
            $table->integer('followers')->default(0);
            $table->string('carrier_details')->nullable();
            $table->string('course_specialization')->nullable();
            $table->string('degree')->nullable();
            $table->string('city');
            $table->string('state');
            $table->string('country');
            $table->string('api_token')->nullable()->unique();
            $table->dateTime('api_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
