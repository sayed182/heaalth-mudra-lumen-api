<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->id();
            $table->integer('posting_as_id');
            $table->integer('experience_id');
            $table->enum('gender', ['male', 'female']);
            $table->integer('vacancy_id');
            $table->integer('job_type_id');
            $table->integer('salary_to');
            $table->integer('salary_from');
            $table->string('organisation');
            $table->string('location');
            $table->string('contact_number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
