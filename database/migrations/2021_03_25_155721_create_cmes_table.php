<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cmes', function (Blueprint $table) {
            $table->id();
            $table->integer('posting_as_id');
            $table->enum('organising', ['academy', 'college', 'agency']);
            $table->string('topic');
            $table->string('person_name');
            $table->string('description');
            $table->string('inclusion');
            $table->boolean('offline_online');
            $table->integer('number_of_days');
            $table->string('place');
            $table->string('venue');
            $table->boolean('fees')->default(false);
            $table->string('registration_authority');
            $table->string('contact_number');
            $table->string('email');
            $table->json('photo_id');
            $table->string('service_charges_amount')->default('125');
            $table->string('service_charges_days')->default('5 days');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cmes');
    }
}
