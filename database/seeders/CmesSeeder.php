<?php

namespace Database\Seeders;

use App\Models\Cmes;
use App\Models\PostingAs;
use Database\Factories\CmesFactory;
use Database\Factories\PhotoesFactory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CmesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cmes::factory()->count(10)->create();
    }
}
