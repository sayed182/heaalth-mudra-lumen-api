<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExperienceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('experience')->insert([
           'name' => 'beginners',
        ]);
        DB::table('experience')->insert([
            'name' => 'intermediate',
        ]);
        DB::table('experience')->insert([
            'name' => 'expert',
        ]);
    }
}
