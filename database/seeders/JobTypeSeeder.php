<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JobTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('job_type')->insert([
            'type' => array_rand(['type_one', 'type_two', 'type_three']),
        ]);
        DB::table('job_type')->insert([
            'type' => array_rand(['type_one', 'type_two', 'type_three']),
        ]);
        DB::table('job_type')->insert([
            'type' => array_rand(['type_one', 'type_two', 'type_three']),
        ]);
        DB::table('job_type')->insert([
            'type' => array_rand(['type_one', 'type_two', 'type_three']),
        ]);
    }
}
