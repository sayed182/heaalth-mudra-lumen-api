<?php

namespace Database\Seeders;

use App\Models\Photoes;
use Illuminate\Database\Seeder;

class PhotoesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Photoes::factory()->count(50)->create();
    }
}
