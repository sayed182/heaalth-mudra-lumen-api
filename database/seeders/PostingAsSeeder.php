<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostingAsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posting_as')->insert([
           'name' => 'admin',
        ]);
        DB::table('posting_as')->insert([
            'name' => 'guest',
        ]);
    }
}
