<?php

namespace Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class VacancySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vacancy')->insert([
           'name' => Str::random('10'),
            'quantity' => 10,
        ]);
        DB::table('vacancy')->insert([
            'name' => Str::random('10'),
            'quantity' => 5,
        ]);
        DB::table('vacancy')->insert([
            'name' => Str::random('10'),
            'quantity' => 16,
        ]);
    }
}
