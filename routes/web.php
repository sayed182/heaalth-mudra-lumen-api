<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->post('api/v1/register', ['uses' => 'UserController@create']);

$router->group(['prefix' => 'api/v1/'], function () use ($router){
    $router->get('/', function (){
       return "Hello";
    });

    $router->group(['prefix' => 'user','middleware'=>'auth'], function () use ($router){
        $router->get('/', ['uses' => 'UsersController@index'] );
        $router->get('/{id}', ['uses' => 'UsersController@getById'] );
        $router->post('/update/{id}', ['uses' => 'UsersController@update'] );
        $router->post('/delete/{id}', ['uses' => 'UsersController@delete'] );
    });

    $router->group(['prefix' => 'posts', 'middleware'=>'auth'], function () use ($router){
        $router->get('/', ['uses' => 'PostsController@index'] );
        $router->get('/{id}', ['uses' => 'PostsController@getById'] );
        $router->post('/update/{id}', ['uses' => 'PostsController@update'] );
        $router->post('/delete/{id}', ['uses' => 'PostsController@delete'] );
        $router->post('/create', ['uses' => 'PostsController@create'] );
    });

    $router->group(['prefix' => 'jobs'], function () use ($router){
        $router->get('/', ['uses' => 'JobsController@index'] );
        $router->get('/{id}', ['uses' => 'JobsController@getById'] );
        $router->post('/update/{id}', ['uses' => 'JobsController@update'] );
        $router->post('/delete/{id}', ['uses' => 'JobsController@delete'] );
        $router->post('/create', ['uses' => 'JobsController@create'] );
    });

    $router->group(['prefix' => 'cmes'], function () use ($router){
        $router->get('/', ['uses' => 'CmesController@index'] );
        $router->get('/{id}', ['uses' => 'CmesController@getById'] );
        $router->post('/update/{id}', ['uses' => 'CmesController@update'] );
        $router->post('/delete/{id}', ['uses' => 'CmesController@delete'] );
        $router->post('/create', ['uses' => 'CmesController@create'] );
    });

    $router->group(['prefix' => 'photoes'], function () use ($router){
        $router->post('/upload', ['uses' => 'PhotoesController@upload'] );
        $router->get('/{id}', ['uses' => 'PhotoesController@getById'] );
        $router->post('/update/{id}', ['uses' => 'PhotoesController@update'] );
        $router->post('/delete/{id}', ['uses' => 'PhotoesController@delete'] );
    });



} );
